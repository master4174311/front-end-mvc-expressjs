function updateAddress() {
  // Prompt the user to enter a new address
  const newAddress = prompt('Enter your new address:');

  // Check if the user entered a new address and it's not empty
  if (newAddress !== null && newAddress.trim() !== '') {
    // Update the address displayed on the page
    document.getElementById('userAddress').textContent = newAddress;

    // You can also update the address in the backend using AJAX if needed
    // For example, send an AJAX request to update the address in the database
    // and handle the response accordingly
  }
}
// Function to confirm the order
function confirmOrder() {

let paymentMethod;
  const paymentOptions = document.getElementsByName('paymentMethod');
  for (let i = 0; i < paymentOptions.length; i++) {
    if (paymentOptions[i].checked) {
      paymentMethod = paymentOptions[i].value;
      break;
    }
  }
  const userName = document.getElementById('userName').innerText;
  const userEmail = document.getElementById('userEmail').innerText;
  const userPhone = document.getElementById('userPhone').innerText;

  const userAddress = document.getElementById('userAddress').innerText;
  const totalPrice = document.getElementById('price').innerText;


  const cartTable = document.getElementById('tableConfirm');
  const rows = cartTable.getElementsByTagName('tr');

  // Initialize an empty array to store the row data
  const rowData = [];

  // Loop through each row (skipping the header row)
  for (let i = 1; i < rows.length; i++) {
    const row = rows[i];

    // Get data from each cell in the row
    const index = row.cells[0].innerText;
    const id = document.getElementById(`productId-${index}`).innerText;

    const name = document.getElementById(`productName-${index}`).innerText;
    const price =parseFloat( document.getElementById(`productPrice-${index}`).getAttribute('data-price'));
    const quantity = document.getElementById(`productQuantity-${index}`).innerText;
    // const total = document.getElementById(`productTotal-${index}`).innerText;
console.log(price)



    // Push the row data into the array
    rowData.push({
      id,
      index: index,
      name: name,
      price: price,
      quantity: quantity,
      // total: total
    });
  }


  const bills = {
    name: userName,
    email: userEmail,
    phone: userPhone,
    addressShipping: userAddress,
    totalPrice,
    paymentMethod:paymentMethod,
    details: rowData
  }


  fetch(`/insert-order`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ bills })
  })
    .then(response => {
      if (response.ok) {
        

        // Nếu phản hồi thành công, cập nhật giỏ hàng trên giao diện người dùng
        // updateCart();

        const newTab = window.open('', '_blank');
        newTab.document.write('<!DOCTYPE html><html><head><title>Order Details</title></head><body></body></html>');
        newTab.document.body.innerHTML = `
          <h1>Thông Tin Hoá Đơn</h1>
          <div>
          <p id="userName">Tên khách hàng: ${userName}</p>
          <p id="userPhone">Số điện thoại: ${userPhone}</p>
      
          <p  id="userEmail">Email: ${userEmail}</p>
          <p id="userAddress"><span id="userAddress">${userAddress}</span></p>
          </div>
          <table border="1">
            <thead>
              <tr>
                <th>#</th>
                <th>Tên sản phẩm</th>
                <th>Giá</th>
                <th>Số lượng</th>
              </tr>
            </thead>
            <tbody>
              ${rowData.map(row => `
                <tr>
                  <td>${row.index}</td>
                  <td>${row.name}</td>
                  <td>${row.price}</td>
                  <td>${row.quantity}</td>
                </tr>
              `).join('')}
            </tbody>
          </table>
          <div>
        <h2>Tổng tiền</h2>
                        <p id="price" class="price-value" data-price="${totalPrice}">Total: ${totalPrice}</p>
      
      </div>
      <script src="/js/price.js"></script>
      
        `;
        newTab.document.close();
        window.location.href = '/checkout/confirm-order';

      } else {
        // Nếu có lỗi, hiển thị thông báo cho người dùng
        console.error('Failed to remove product from cart');
      }
    })
    .catch(error => {
      // Nếu có lỗi trong quá trình gửi yêu cầu, hiển thị thông báo cho người dùng
      console.error('Error removing product from cart:', error);
    });


}
function backConfirmOrder() {
  // Implement logic to confirm the order here
  // For example, you can send an AJAX request to the server to finalize the order
  console.log('Confirm order clicked');
  window.location.href = '/cart'; // Redirect to checkout page

}