module.exports = (app) => {
  app.get('/admin/home/categories', (req, res) => {
    
    setTimeout(() => {
      res.render('admin/categories.hbs', {
        title: 'Manage Categories',
        layout: 'admin_layout' // Thay đổi layout thành 'admin_layout'

      });
    }, 100);
  });
};
