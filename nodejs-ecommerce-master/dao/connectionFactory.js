const mysql = require('mysql');

function connectionFactory() {
  const connection = mysql.createConnection({
    host: process.env.DB_HOST || 'localhost',
    user: process.env.DB_USER || 'root',
    password: process.env.DB_PASSWORD || '123456',
    database: process.env.DB_NAME || 'ecommerce',
    port: process.env.DB_PORT || '3306'
  });
// Attempt to connect
// connection.connect(function(err) {
//   if (err) {
//     console.error('Error connecting to database:', err);
//     return;
//   }
//   console.log('Connected to database successfully');
  
//   // Close the connection
//   connection.end(function(err) {
//     if (err) {
//       console.error('Error closing connection:', err);
//       return;
//     }
//     console.log('Connection closed');
//   });
// }); 
 return connection;
}

module.exports = () => connectionFactory;
