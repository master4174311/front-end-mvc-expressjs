document.addEventListener("DOMContentLoaded", function() {
    
    console.log("Login header clicked");

    // Find the loginHeader element by its ID
    const loginHeader = document.getElementById("loginHeader");

    // Add a click event listener to the loginHeader element
    loginHeader.addEventListener("click", function() {
        // Perform the desired action when the loginHeader is clicked
        // For example, you can open a login modal or navigate to a login page
        // Here, we'll just log a message to the console
        console.log("Login header clicked");
    });
});
