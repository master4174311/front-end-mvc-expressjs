module.exports = (app) => {
  app.get('/log-out', (req, res) => {
    req.session['cart'] = null;
    res.redirect('/');
  });
};
