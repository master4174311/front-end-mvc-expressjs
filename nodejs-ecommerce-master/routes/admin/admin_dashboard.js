module.exports = (app) => {
  app.get('/admin/home', (req, res) => {
    
    setTimeout(() => {
      res.status(200).render('admin/admin_dashboard.hbs', {
        title: 'Admin Dashboard',
        layout: 'admin_layout' // Thay đổi layout thành 'admin_layout'

      });
    }, 100);
  });
};
