class categoriesDAO {
  constructor(connection) {
    this.connection = connection;
  }
  list(limit=null) {
    return new Promise((resolve, reject) => {
      if (limit) {
        this.connection
            .query('select * from categories limit ?', limit,
                (err, result) => {
                  if (err) return reject(err);
                  return resolve(result);
                });
      }
      this.connection
          .query('select * from categories',
              (err, result) => {
                if (err) return reject(err);
                return resolve(result);
              });
    });
  }
}

module.exports = () => categoriesDAO;
