import { readFileSync, writeFileSync } from 'fs';
import { autoScroll, waitForTimeout, DEFAULT_TIMEOUT, generateRandomRating } from "./common.js";
import { v4 as uuidv4 } from 'uuid';

const outputFilePath = 'products_detail.json'; // Path to the output JSON file

(async () => {
    const products = [];
    const sql = [];
    const datas = await readFileSync('products_laptop_2.json', 'utf-8');
    const datasproduct = await readFileSync('product.json', 'utf-8');

    const parseJson = JSON.parse(datas);

    const parseJsonpr = JSON.parse(datasproduct);

    // parseJson.forEach((element, index) => {
    //     const { name, image, rating, configParams, promoProgress, promoStrikePrice, priceValue } = element;
    //     parseJsonpr.forEach((item, index2) => {
    //         const { id, name, rating, img_url } = item;
    //         if (name === name && rating === rating && image === img_url) {
    //             const query = `(uuid(),'${configParams.CPU}', 'processor.png', '${id}', 'admin', CURDATE(), '', NOW())`
    //             //  const query1 =  `(uuid(),'${configParams.RAM}', 'ram.png', '${id}', 'admin', CURDATE(), '', NOW())`
    //             //  const query2 =  `(uuid(),'${configParams.Screen}', 'screen.png', '${id}', 'admin', CURDATE(), '', NOW())`
    //             //  const query3 =  `(uuid(),'${configParams.Graphics}', 'vga-card.png', '${id}', 'admin', CURDATE(), '', NOW())`
    //             //  const query4 =  `(uuid(),'${configParams.Weight}', 'kilogram.png', '${id}', 'admin', CURDATE(), '', NOW())`
    //             //  const query5 =  `(uuid(),'${configParams.Storage}', 'harddisk.png', '${id}', 'admin', CURDATE(), '', NOW())`

    //             // const priceLast1 = promoProgress === ''|| promoProgress === null ?0  : promoProgress.replace(/\d+ ngày \d+:\d+:\d+$/, '').replace(" ₫", "").replace(".", "").replace(".", "")  
    //             // const priceLast2 = promoStrikePrice === ''|| promoStrikePrice === null ? 0   : promoStrikePrice.replace(/\d+ ngày \d+:\d+:\d+$/, '').replace(" ₫", "").replace(".", "").replace(".", "") 
    //             // const priceLast3 = priceValue === ''|| priceValue === null ? 0 : priceValue.replace(/\d+ ngày \d+:\d+:\d+$/, '').replace(" ₫", "").replace(".", "").replace(".", "") 

    //             //  const query5 =  `(uuid(),${priceLast1}, ${priceLast2}, ${priceLast3}, '${name}',${rating}, '${image}')`

    //             sql.push(query)
    //             // sql.push(query1)
    //             // sql.push(query2)
    //             // sql.push(query3)
    //             // sql.push(query4)
    //             // sql.push(query5)

    //         }

    //     })
    // });


    parseJson.forEach((element, index) => {
        const { name, image, rating, configParams, promoProgress, promoStrikePrice, priceValue } = element;
        parseJsonpr.forEach((item, index2) => {
            const { id, name: itemName, rating: itemRating, img_url } = item; // Sử dụng tên biến khác để tránh xung đột
            if (name === itemName && rating === itemRating && image === img_url) {
                const query = `(uuid(),'${configParams.Storage}', 'harddisk.png', '${id}', 'admin', CURDATE(), '', NOW())`;
                sql.push(query);
            }
        });
    });
    await writeFileSync(outputFilePath, JSON.stringify(sql, null, 2));

})();
