
var {checkSafety} = require('../../helpers/modelsAPI');
var isTurnOnCheckSercurity = require('../../helpers/constants');

module.exports = (app) => {

  app.get('/sign-up', (req, res) => {
    let success; const warning = app.helpers.msg(req);
    let totalQuantity;
    // if (req.session['cart'] || req.session['cart'] != null) {
    //   req.session['warning'] = 'You are not able to access this area!';
    //   return res.redirect('/');
    // }
    const isCheckSession = req.session['cart']

    if (isCheckSession === undefined || isCheckSession === null || (isCheckSession.quantity === 0 && isCheckSession.user === null)) {
      req.session['cart'] = {
        user: null,
        cartId: [],
        products: [],
        totalQuantity: 0
      };
    } else {

      totalQuantity = isCheckSession.products.reduce((total, item) => total + item.quantity, 0);

    }
    res.render('sign/up', {
      title: 'Sign Up',
      success, warning,
      totalQuantity
      // csrfToken: req.csrfToken(),
    });
  });

//   app.post('/sign-up', async (req, res) => {

//     const username = req.body.username;
//     const email = req.body.email;
//     const password = req.body.password;
//     const phone = req.body.phone;
//     const address = req.body.address;
//     const payment_type = req.body?.paymentMethod;

//     let totalQuantity;

//     const user = {
//       username,
//       email,
//       password,
//       type: 0,
//       phone,
//       address,
//       addressShipping: address,
//       payment_type

//     }
//     // req.checkBody('username', 'Username is Empty').notEmpty().isLength({min: 4});
//     // req.checkBody('email', 'Email is not Valid').notEmpty().isEmail();
//     // req.checkBody('password', 'Password must be at least 4 digits').notEmpty();
//     // const errosInValidation = req.validationErrors();
//     // if (errosInValidation) {
//     //   req.session['warning'] = errosInValidation[0].msg;
//     //   res.redirect('/sign-up');
//     // };
//     const isCheckSession = req.session['cart']

//     if (isCheckSession === undefined || isCheckSession === null || (isCheckSession.quantity === 0 && isCheckSession.user === null)) {
//       req.session['cart'] = {
//         user: null,
//         cartId: [],
//         products: [],
//         totalQuantity: 0
//       };
//     } else {

//       totalQuantity = isCheckSession.products.reduce((total, item) => total + item.quantity, 0);

//     }
//     const connection = app.dao.connectionFactory();
//     const UserDao = new app.dao.userDAO(connection);

//     UserDao.saveUser(user)
//       .then((result) => {
//         req.session['success'] = result;

//       })
//       .catch((err) => {
//         req.session['warning'] = err;
//         return res.redirect('/');
        

//       });


//     if (req.session['cart'] === null || req.session['cart'] === "" || req.session['cart'] === undefined) {
//       req.session['cart'] = {
//         user: user,
//         cartId: [],
//         products: [],
//         totalQuantity: 0
//       };
//       res.redirect('/');
// return
//     }
//     else {
//       req.session['cart'].user = user
    
//       req.session['success'] = 'Đăng ký tài khoản thành công! Vui lòng đăng nhập lại ';

//       return res.redirect('/')
//       // return res.redirect(`/user/${customer.id}`);

//       // return res.status(200).render('customer/index', {
//       //   title: 'Customer Dashboard',
//       //   user: user,
//       //   totalQuantity,
//       //   orders: orders // Pass the orders data to the view
//       // });


//     }
//     return;
//   });
app.post('/sign-up', async (req, res) => {
  const username = req.body.username;
  const email = req.body.email;
  const password = req.body.password;
  const phone = req.body.phone;
  const address = req.body.address;
  const payment_type = req.body?.paymentMethod;
  if (isTurnOnCheckSercurity === 1) {
  const isSafe = await checkSafety([username, password,phone,address,payment_type]);

  if (isSafe === 0) {
    req.session['warning'] = 'Nội dung có chứa mã độc';
    res.redirect('/'); // Redirect to home page if unsafe
    return;
  }

  }

  let totalQuantity;

  const user = {
    username,
    email,
    password,
    type: 0,
    phone,
    address,
    addressShipping: address
  };

  const isCheckSession = req.session['cart'];

  if (isCheckSession === undefined || isCheckSession === null || (isCheckSession.quantity === 0 && isCheckSession.user === null)) {
    req.session['cart'] = {
      user: null,
      cartId: [],
      products: [],
      totalQuantity: 0
    };
  } else {
    totalQuantity = isCheckSession.products.reduce((total, item) => total + item.quantity, 0);
  }

  const connection = app.dao.connectionFactory();
  const UserDao = new app.dao.userDAO(connection);

  try {
    await UserDao.saveUser(user);
    req.session['warning'] = 'Đăng ký tài khoản thành công! Vui lòng đăng nhập lại ';
    res.redirect('/sign-in');
  } catch (err) {
    req.session['warning'] = err;
    res.redirect('/sign-up');
  }
});


};
async function getCustomer(email, userDao) {
  try {

    // Await the result of the login operation
    const result = await userDao.getCustomer(email);
    return result[0]; // Assign the result to the user variable
    // Any subsequent operations depending on user should also be placed here
  } catch (err) {
    console.log(err)
  }
}
async function getOrdersByEmail(userId, orderDao) {
  try {

    // Await the result of the login operation
    const result = await orderDao.getOrdersByEmail(userId);

    return result; // Assign the result to the user variable
    // Any subsequent operations depending on user should also be placed here
  } catch (err) {
    console.log(err)
  }

}
