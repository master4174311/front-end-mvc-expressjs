class productsDAO {
  constructor(connection) {
    this.connection = connection;
  }
  list(limit=null) {
    return new Promise((resolve, reject) => {
      if (limit) {
        this.connection
            .query('SELECT p.*,pa.base_price,pa.discount_price,pa.promotion_price FROM products p INNER JOIN product_price   pa ON pa.product_id = p.id LIMIT ?', limit,
                (err, result) => {
                  if (err) return reject(err);
                  return resolve(result);
                }
            );
      }
      // End Queries for limits
      else {
        this.connection
            .query(`select *, (select GROUP_CONCAT(  '[', JSON_OBJECT('name_config',pd.name,'image_config',pd.img_url) ,']' ) as 'config' from product_detail pd where product_id in (p.id) ) as 'config' from products p`,
                (err, result) => {
                  if (err) return reject(err);
                  return resolve(result);
                }
            );
      }
    });
  }

  listProductsWithPagination(start, length) {
    return new Promise((resolve, reject) => {
        // Perform SQL query with pagination
        const query = 'SELECT SQL_CALC_FOUND_ROWS id, name, rating, price, img_url, quantity, creation_by, creation_time, last_modified_by, last_modified_time LIMIT ?, ?';
        this.connection.query(query, [start, length], (err, result) => {
            if (err) {
                return reject(err);
            }
            // Query total records count
            this.connection.query('SELECT FOUND_ROWS() AS total', (err, countResult) => {
                if (err) {
                    return reject(err);
                }
                const totalRecords = countResult[0].total;
                resolve({
                    totalRecords: totalRecords,
                    filteredRecords: totalRecords, // In this example, total and filtered records are the same
                    data: result
                });
            });
        });
    });
}
listOrdersWithPagination(start, length) {
  return new Promise((resolve, reject) => {
      // Perform SQL query with pagination
      const query = 'SELECT SQL_CALC_FOUND_ROWS  id, bill_no, adress_shipping, user_id, status, payment_type, creation_by, creation_time, last_modified_by, last_modified_time  from orders o LIMIT ?, ?';
      this.connection.query(query, [start, length], (err, result) => {
          if (err) {
              return reject(err);
          }
          // Query total records count
          this.connection.query('SELECT FOUND_ROWS() AS total', (err, countResult) => {
              if (err) {
                  return reject(err);
              }
              const totalRecords = countResult[0].total;
              resolve({
                  totalRecords: totalRecords,
                  filteredRecords: totalRecords, // In this example, total and filtered records are the same
                  data: result
              });
          });
      });
  });
}

listCategoriesWithPagination(start, length) {
  return new Promise((resolve, reject) => {
      // Perform SQL query with pagination
      const query = 'SELECT SQL_CALC_FOUND_ROWS  id, name, image, creation_by, creation_time, last_modified_by, last_modified_time  from categories   LIMIT ?, ?';
      this.connection.query(query, [start, length], (err, result) => {
          if (err) {
              return reject(err);
          }
          // Query total records count
          this.connection.query('SELECT FOUND_ROWS() AS total', (err, countResult) => {
              if (err) {
                  return reject(err);
              }
              const totalRecords = countResult[0].total;
              resolve({
                  totalRecords: totalRecords,
                  filteredRecords: totalRecords, // In this example, total and filtered records are the same
                  data: result
              });
          });
      });
  });
}

  orderedList(order=null) {
    return new Promise((resolve, reject) => {
      if (order == 'low-price') {
        this.connection.query('SELECT p.*,pa.base_price,pa.discount_price,pa.promotion_price FROM products p INNER JOIN product_price   pa ON pa.product_id = p.id ORDER BY p.price ASC',
            (err, result) => {
              if (err) return reject(err);
              return resolve(result);
            });
      }
      this.connection.query('SELECT p.*,pa.base_price,pa.discount_price,pa.promotion_price FROM products p INNER JOIN product_price   pa ON pa.product_id = p.id ORDER BY ?? DESC', order,
          (err, result) => {
            if (err) return reject(err);
            return resolve(result);
          });
    });
  }
  filteredList(filter) {
    return new Promise((resolve, reject) => {

    });
  }
  getByIds(ids) {
    return new Promise((resolve, reject) => {
      const formattedIds = ids.map(result => `'${result.id}'`).join(',');

      this.connection.query(`select p.* from product_category pc inner join products p on pc.product_id = p.id inner join categories c on c.id = pc.category_id where p.id in (${formattedIds})`,
          (err, result) => {
            if (err) return reject(err);
            return resolve(result);
          });
    });
  }
  getById(id) {
    return new Promise((resolve, reject) => {

      this.connection.query(`select p.* from product_category pc inner join products p on pc.product_id = p.id inner join categories c on c.id = pc.category_id where p.id = ?`,id,
          (err, result) => {
            if (err) return reject(err);
            return resolve(result);
          });
    });
  }

  getById2(id) {
    return new Promise((resolve, reject) => {

      this.connection.query(`SELECT p.id AS product_id, p.name AS product_name, p.rating AS product_rating, p.img_url AS product_image, pp.base_price, pp.discount_price, pp.promotion_price, a.name AS attribute_name, pa.value AS attribute_value FROM products p INNER JOIN product_price pp ON p.id = pp.product_id INNER JOIN product_attributes pa ON pa.product_id = p.id INNER JOIN attributes a ON a.id = pa.attribute_id WHERE p.id = ?`, [id],
            (err, result) => {
                if (err) return reject(err);
                return resolve(result);
            });
    });
  }
  getByCategoryId(id){
    return new Promise((resolve,reject)=>{
      this.connection.query('select p.* from product_category pc inner join products p on pc.product_id = p.id inner join categories c on c.id = pc.category_id where pc.category_id = ?',id,(err,result)=>{
        if (err) {
          // Reject the promise if there's an error
          return reject(err);
        }
        // Resolve the promise with the query result
        resolve(result);
      });
    })
  }
  getByProductName(key) {
    return new Promise((resolve, reject) => {
        this.connection.query(`SELECT * FROM products p INNER JOIN product_price   pa ON pa.product_id = p.id WHERE p.name LIKE ?`, [`%${key}%`], (err, result) => {
            if (err) {
                // Reject the promise if there's an error
                return reject(err);
            }
            // Resolve the promise with the query result
            resolve(result);
        });

        
    });
}
getAllProducts() {
  return new Promise((resolve, reject) => {
    // const query = `
    //   SELECT
    //     p.id,
    //     p.name,
    //     p.rating,
    //     pp.base_price,
    //     pp.discount_price,
    //     pp.promotion_price,
    //     MAX(CASE WHEN a.name = 'CPU' THEN pa.value END) AS cpu,
    //     MAX(CASE WHEN a.name = 'Graphics' THEN pa.value END) AS graphics,
    //     MAX(CASE WHEN a.name = 'RAM' THEN pa.value END) AS ram,
    //     MAX(CASE WHEN a.name = 'Screen' THEN pa.value END) AS screen,
    //     MAX(CASE WHEN a.name = 'Storage' THEN pa.value END) AS storage,
    //     MAX(CASE WHEN a.name = 'Weight' THEN pa.value END) AS weight
    //   FROM
    //     product_attributes pa
    //   INNER JOIN
    //     attributes a ON a.id = pa.attribute_id
    //   INNER JOIN
    //     products p ON p.id = pa.product_id
    //   INNER JOIN
    //     product_price pp ON pp.product_id = p.id
    //   GROUP BY
    //     p.id, p.name, p.rating, pp.base_price, pp.discount_price, pp.promotion_price
    // `;

    const query = 'SELECT p.*,pa.base_price,pa.discount_price,pa.promotion_price FROM products p INNER JOIN product_price   pa ON pa.product_id = p.id ORDER BY p.price ASC'

    this.connection.query(query, (err, result) => {
      if (err) {
        // Reject the promise if there's an error
        return reject(err);
      }
      // Resolve the promise with the query result
      resolve(result);
    });
  });
}







}

module.exports = () => productsDAO;
