module.exports = (app) => {
  app.get('/admin', (req, res) => {
    let success; const warning = app.helpers.msg(req);

    
    setTimeout(() => {
      res.status(200).render('admin/index', {
        title: 'Admin',
        user: req.session['admin'],
        layout: 'admin_login_layout', // Thay đổi layout thành 'admin_layout'
        // csrfToken: req.csrfToken(),

      });
    }, 100);
  });

  app.post('/admin/login', (req, res) => {
    const email = req.body.email;
    const password = req.body.password;
    const connection = app.dao.connectionFactory();
    req.checkBody('email', 'Email is not Valid!').notEmpty().isEmail();
    req.checkBody('password', 'Password must be at least 4 digits!')
      .notEmpty().isLength({ min: 4 });
    const errorsInValidation = req.validationErrors();
    if (errorsInValidation) {
      req.session['warning'] = errorsInValidation[0].msg;
      res.redirect('/');
    }

    const connection2 = app.dao.connectionFactory();
    const userDao = new app.dao.userDAO(connection2);

    userDao.login(email, password)
      .then((result) => {
        req.session['success'] = result;
        // Create Session
        req.session['cart'] = {
          username: 'username',
          email: email,
          admin: false,
          cart: null,
        };

        res.redirect('/');
      })
      .catch((err) => {
        req.session['warning'] = err;
        res.redirect('/');
      });
  });



};



