
var {checkSafety} = require('../../helpers/modelsAPI');
var isTurnOnCheckSercurity = require('../../helpers/constants');

module.exports = (app) => {
  app.get('/user/:userId', async (req, res) => {
    const userId = req.params.userId;
    const connection = app.dao.connectionFactory();

    const userDao = new app.dao.userDAO(connection);
    const orderDao = new app.dao.orderDAO(connection);

    let totalQuantity = 0;
    const isCheckSession = req.session['cart']

    if (isCheckSession === undefined || isCheckSession === null || (isCheckSession.quantity === 0 && isCheckSession.user === null)) {
      req.session['cart'] = {
        user: null,
        cartId: [],
        products: [],
        totalQuantity: 0
      };
    } else {

      totalQuantity = isCheckSession.products.reduce((total, item) => total + item.quantity, 0);

    }

    const user = await userDao.getUserById(userId);
    if (!user) {
      return
    }
    req.session['cart'].user = user

    orders = await getOrders(userId, orderDao);

    setTimeout(() => {
      res.status(200).render('customer/index', {
        title: 'Customer Dashboard',
        user: user[0],
        totalQuantity,
        orders: orders // Pass the orders data to the view
      });
    }, 100);
  });


  app.get('/sign-in', (req, res) => {
    let success; const warning = app.helpers.msg(req);

    // if (req.session['cart'] || req.session['cart'] != null) {
    //   req.session['warning'] = 'You are not able to access this area!';
    //   return res.redirect('/');
    // }
    let totalQuantity = 0;
    const isCheckSession = req.session['cart']

    if (isCheckSession === undefined || isCheckSession === null || (isCheckSession.quantity === 0 && isCheckSession.user === null)) {
      req.session['cart'] = {
        user: null,
        cartId: [],
        products: [],
        totalQuantity: 0
      };
    } else {

      totalQuantity = isCheckSession.products.reduce((total, item) => total + item.quantity, 0);

    }
    if (!(isCheckSession?.quantity === 0 && isCheckSession?.user === null)) {
      let user = isCheckSession?.user

      res.render('sign/in', {
        title: 'Sign In',
        success, warning,
        totalQuantity,
        user
        // csrfToken: req.csrfToken(),
      });
      return;
    }
    res.render('sign/in', {
      title: 'Sign In',
      success, warning,
      totalQuantity

      // csrfToken: req.csrfToken(),
    });
  });

  app.post('/sign-in', async (req, res) => {
    let user; let orders;
    let totalQuantity = 0;
    const email = req.body.email;
    const password = req.body.password;
    const connection = app.dao.connectionFactory();
    const isCheckSession = req.session['cart']

    if (isCheckSession === undefined || isCheckSession === null || (isCheckSession.quantity === 0 && isCheckSession.user === null)) {
      req.session['cart'] = {
        user: null,
        cartId: [],
        products: [],
        totalQuantity: 0
      };
    } else {

      totalQuantity = isCheckSession.products.reduce((total, item) => total + item.quantity, 0);

    }


    // req.checkBody('email', 'Email is not Valid!').notEmpty().isEmail();
    // // req.checkBody('password', 'Password must be at least 4 digits!')
    // //     .notEmpty().isLength({min: 4});
    // const errorsInValidation = req.validationErrors();
    // if (errorsInValidation) {
    //   req.session['warning'] = errorsInValidation[0].msg;
    //   res.redirect('/sign-in');
    //   return;
    // }

    const userDao = new app.dao.userDAO(connection);

    const orderDao = new app.dao.orderDAO(connection);
    if (isTurnOnCheckSercurity === 1) {
      const isSafe = await checkSafety([password]);
      if (isSafe === 0) {
        req.session['warning'] = `Nội dung có chứa mã độc, Bạn đã nhập: ${password} `;
        return res.redirect('/sign-in'); // Return after redirect
      }
    }

    user = await loginUser(email, password, userDao);
    if (user === null || !user) {
      req.session['warning'] = 'Tài khoản hoặc mật khẩu không đúng';
      return res.redirect('/sign-in'); // Return after redirect
    }

    if (!req.session['cart']) {
      req.session['cart'] = {
        user: user,
        cartId: [],
        products: [],
        totalQuantity: 0
      }
    }
    req.session['cart'].user = user
    // This will log undefined or the previous value of user because the .then block hasn't executed yet


    orders = await getOrders(user.id, orderDao);
    setTimeout(() => {
      res.status(200).render('customer/index', {
        title: 'Customer Dashboard',
        user: user,
        totalQuantity,
        orders: orders // Pass the orders data to the view
      });
    }, 100);
  });

}

async function loginUser(email, password, userDao) {
  try {
    if (isTurnOnCheckSercurity === 1) {
      const isSafe = await checkSafety([password]);
      if (isSafe === 0) {
        req.session['warning'] = 'Nội dung có chứa mã độc';
        res.redirect('/'); // Redirect to home page if unsafe
        return;
      }
    }
    // Await the result of the login operation
    const result = await userDao.login(email, password);
    if (result === null) {
      return null;
    }
    return result[0]; // Assign the result to the user variable
    // Any subsequent operations depending on user should also be placed here
  } catch (err) {
    console.log(err);
    return null;
  }
}

async function getOrders(userId, orderDao) {
  try {

    // Await the result of the login operation
    const result = await orderDao.getOrders(userId);

    return result; // Assign the result to the user variable
    // Any subsequent operations depending on user should also be placed here
  } catch (err) {
    console.log(err)
  }

}
