module.exports = (app) => {
    app.get('/checkout/confirm', (req, res) => {
        const products = req.session['cart'] ? req.session['cart'].products : [];
        const totalQuantity = products.reduce((total, item) => total + item.quantity, 0);
        

const user = req.session['cart'].user

        let success; const warning = app.helpers.msg(req);
        const connection = app.dao.connectionFactory();
        const productsDao = new app.dao.productsDAO(connection);


        if (products.length == 0) {
            res.render('/', {
                title: 'Home',
                warning: 'You do not have items in your cart!',
            });

        }
        const totalPrice = products.reduce((acc, item) => acc + (item.product[0].price * 1), 0);
        res.render('confirm/index', {
            title: 'Cart',
            success, warning, user: req.session['cart'].user,
            totalQuantity, totalPrice,
            products,
            user:user
        });





    });
    // Route to handle order confirmation
    app.get('/checkout/confirm-order', (req, res) => {
        let totalQuantity
        const isCheckSession = req.session['cart'] 
        const user = req.session['cart'].user

    if (isCheckSession === undefined || isCheckSession === null || isCheckSession.quantity === 0 ) {
      req.session['cart'] = {
        user: null,
        cartId: [],
        products: [],
        totalQuantity: 0
    };
    }else{
  
       totalQuantity = isCheckSession.products.reduce((total, item) => total + item.quantity, 0);
  
    }
        // Assuming you retrieve payment method and other details from the request body
        const { paymentMethod, address, /* other details */ } = req.body;

        // Insert order into the database
        const orderDetails = {
            paymentMethod: paymentMethod,
            address: address,
            // Add other order details as needed
        };
        req.session['cart'] = {
            user:user,
            cartId: [],
            products: [],
            totalQuantity: 0}
        res.render('thanks/index', {
            title: 'Thank You',
            totalQuantity,
            user
        });
        
    });
    app.post('/insert-order', (req, res) => {
        const { bills } = req.body; // Assuming 'bills' contains the necessary data for insertion
    console.log(bills)
        // Insert logic for orders and order transactions here
        const connection = app.dao.connectionFactory();
        const orderDao = new app.dao.orderDAO(connection);
        orderDao.saveOrders(bills,req.session['cart'].user.id)
        // Example response
        res.status(200).send("success")
    });
    
}
