-- ecommerce.`attributes` definition

CREATE TABLE `attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `icons` varchar(255) NOT NULL DEFAULT '',
  `creation_by` varchar(255) DEFAULT 'system',
  `creation_time` timestamp NULL DEFAULT current_timestamp(),
  `last_modified_by` varchar(255) DEFAULT 'system',
  `last_modified_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;


-- ecommerce.categories definition

CREATE TABLE `categories` (
  `id` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `creation_by` varchar(255) DEFAULT 'system',
  `creation_time` timestamp NULL DEFAULT current_timestamp(),
  `last_modified_by` varchar(255) DEFAULT 'system',
  `last_modified_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;


-- ecommerce.order_transaction definition

CREATE TABLE `order_transaction` (
  `id` varchar(36) NOT NULL,
  `order_id` varchar(36) NOT NULL,
  `product_id` varchar(36) NOT NULL,
  `quantity` int(11) NOT NULL,
  `ammount` decimal(10,0) NOT NULL,
  `creation_by` varchar(255) DEFAULT 'system',
  `creation_time` timestamp NULL DEFAULT current_timestamp(),
  `last_modified_by` varchar(255) DEFAULT 'system',
  `last_modified_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;


-- ecommerce.orders definition

CREATE TABLE `orders` (
  `id` varchar(36) NOT NULL,
  `bill_no` varchar(255) NOT NULL,
  `adress_shipping` varchar(255) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `status` varchar(255) NOT NULL,
  `payment_type` varchar(255) NOT NULL,
  `creation_by` varchar(255) DEFAULT 'system',
  `creation_time` timestamp NULL DEFAULT current_timestamp(),
  `last_modified_by` varchar(255) DEFAULT 'system',
  `last_modified_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;


-- ecommerce.product_attributes definition

CREATE TABLE `product_attributes` (
  `id` varchar(36) NOT NULL,
  `product_id` varchar(36) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;


-- ecommerce.product_category definition

CREATE TABLE `product_category` (
  `id` varchar(36) NOT NULL,
  `product_id` varchar(36) NOT NULL,
  `category_id` varchar(36) NOT NULL,
  `creation_by` varchar(255) DEFAULT 'system',
  `creation_time` timestamp NULL DEFAULT current_timestamp(),
  `last_modified_by` varchar(255) DEFAULT 'system',
  `last_modified_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;


-- ecommerce.product_detail definition

CREATE TABLE `product_detail` (
  `id` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `img_url` varchar(255) NOT NULL,
  `product_id` varchar(36) NOT NULL,
  `creation_by` varchar(36) DEFAULT NULL,
  `creation_time` datetime DEFAULT current_timestamp(),
  `last_modified_by` varchar(36) DEFAULT NULL,
  `last_modified_time` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;


-- ecommerce.product_detail_bk definition

CREATE TABLE `product_detail_bk` (
  `id` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `img_url` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `product_id` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `creation_by` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `creation_time` datetime DEFAULT current_timestamp(),
  `last_modified_by` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `last_modified_time` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;


-- ecommerce.product_price definition

CREATE TABLE `product_price` (
  `id` varchar(36) NOT NULL,
  `product_id` varchar(36) NOT NULL,
  `base_price` decimal(10,0) NOT NULL,
  `discount_price` decimal(10,0) DEFAULT 0,
  `promotion_price` decimal(10,0) DEFAULT 0,
  `creation_by` varchar(255) DEFAULT 'system',
  `creation_time` timestamp NULL DEFAULT current_timestamp(),
  `last_modified_by` varchar(255) DEFAULT 'system',
  `last_modified_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;


-- ecommerce.products definition

CREATE TABLE `products` (
  `id` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `rating` decimal(10,0) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `img_url` varchar(255) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `creation_by` varchar(255) DEFAULT 'system',
  `creation_time` timestamp NULL DEFAULT current_timestamp(),
  `last_modified_by` varchar(255) DEFAULT 'system',
  `last_modified_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;


-- ecommerce.sqli_dataset definition

CREATE TABLE `sqli_dataset` (
  `Query` varchar(256) DEFAULT NULL,
  `Label` int(11) DEFAULT NULL,
  `id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;


-- ecommerce.tmp definition

CREATE TABLE `tmp` (
  `id` varchar(36) DEFAULT NULL,
  `promoProgress` decimal(10,0) DEFAULT NULL,
  `promoStrikePrice` decimal(10,0) DEFAULT NULL,
  `priceValue` decimal(10,0) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rating` decimal(10,0) DEFAULT NULL,
  `img_url` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;


-- ecommerce.tmp_config definition

CREATE TABLE `tmp_config` (
  `id` uuid NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `product_id` varchar(50) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` date DEFAULT curdate(),
  `modified_by` varchar(50) DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;


-- ecommerce.users definition

CREATE TABLE `users` (
  `id` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `type` int(11) NOT NULL DEFAULT 0,
  `phone_number` varchar(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `address_shipping` varchar(255) NOT NULL,
  `creation_by` varchar(255) DEFAULT 'system',
  `creation_time` timestamp NULL DEFAULT current_timestamp(),
  `last_modified_by` varchar(255) DEFAULT 'system',
  `last_modified_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;


-- ecommerce.xss_dataset definition

CREATE TABLE `xss_dataset` (
  `id` int(11) DEFAULT NULL,
  `sentence` text DEFAULT NULL,
  `label` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;