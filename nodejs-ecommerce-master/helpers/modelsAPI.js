const axios = require('axios');

async function checkSafety(searchTerm) {
    try {
        const [isSafeSQLI, isSafeXSS] = await Promise.all([
            axios.post(`http://127.0.0.1:5000/api/sqli/check`, {
                "queries": searchTerm,
                "max_len": 100,
                "max_words": 5000
            }),
            axios.post(`http://127.0.0.1:5000/api/xss/check`, {
                "queries": searchTerm,
                "max_len": 100,
                "max_words": 5000
            })
        ]);

        // Check if both SQLI and XSS are safe
        const isSafe = isSafeSQLI.data.results[0].isSafe && isSafeXSS.data.results[0].isSafe;

        console.log("Is safe:", isSafe);
        return isSafe;
    } catch (error) {
        console.error("Error:", error);
        return false;
    }
}

async function getModelsAsync(){
    try {
       const response = await axios.post(`http://127.0.0.1:5000/api/models/info`)
       if (response.status === 200) {
        return response.data;
       }
    } catch (error) {
        console.error("Error:", error);

    }
}


async function getTrainKNNModelsAsync(kCluster){
    try {
        const k = parseInt(kCluster)
       const response = await axios.post(`http://127.0.0.1:5000/api/knn/retrain/${k}`)
       if (response.status === 200) {
        return response.data;
       }
    } catch (error) {
        console.error("Error:", error);

    }
}



async function getTrainSQLiRNNModelsAsync(body){
    try {

       const response = await axios.post(`http://127.0.0.1:5000/api/sqli/retrain`,body)
       if (response.status === 200) {
        return response.data;
       }
    } catch (error) {
        console.error("Error:", error);

    }
}


async function getTrainXSSRNNModelsAsync(body){
    try {

       const response = await axios.post(`http://127.0.0.1:5000/api/xss/retrain`,body)
       if (response.status === 200) {
        return response.data;
       }
    } catch (error) {
        console.error("Error:", error);

    }
}


module.exports = {
    checkSafety,getModelsAsync,getTrainKNNModelsAsync,getTrainSQLiRNNModelsAsync,
    getTrainXSSRNNModelsAsync
};
