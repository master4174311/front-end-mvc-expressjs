module.exports = (app) => {
  app.get('/admin/home/models', (req, res) => {
    
    setTimeout(() => {
      res.render('admin/models.hbs', {
        title: 'Manage Models',
        layout: 'admin_layout' // Thay đổi layout thành 'admin_layout'

      });
    }, 100);
  });
};
