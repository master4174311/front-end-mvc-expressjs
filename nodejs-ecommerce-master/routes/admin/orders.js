module.exports = (app) => {
  app.get('/admin/home/orders', (req, res) => {
    
    setTimeout(() => {
      res.render('admin/orders.hbs', {
        title: 'Manage Orders',
        layout: 'admin_layout' // Thay đổi layout thành 'admin_layout'

      });
    }, 100);
  });
};
