module.exports = (app) => {
  app.get('/admin/home/products', (req, res) => {
    
    setTimeout(() => {
      res.render('admin/products.hbs', {
        title: 'Manage Products',
        layout: 'admin_layout' // Thay đổi layout thành 'admin_layout'

      });
    }, 100);
  });
};
