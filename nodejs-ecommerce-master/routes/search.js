var expressHbs = require('express-handlebars');
var axios = require('axios');
var { checkSafety } = require('../helpers/modelsAPI');
var isTurnOnCheckSercurity = require('../helpers/constants');


module.exports = (app) => {

  var hbs = expressHbs.create({});
  // Định nghĩa helper cho việc so sánh hai giá trị
  hbs.handlebars.registerHelper('ifEquals', function (arg1, arg2, options) {
    return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
  });
  hbs.handlebars.registerHelper('ifGreaterThan', function (arg1, arg2, options) {
    return arg1 > arg2 ? options.fn(this) : options.inverse(this);
  });
  hbs.handlebars.registerHelper('isFirstIndex', function (index) {
    return index % 3 === 0;
  });

  hbs.handlebars.registerHelper('isLastIndex', function (index) {
    return index % 3 === 2 || index === (similarProducts.length - 1);
  });
  // Define Handlebars helper for performing modulus operation
  hbs.handlebars.registerHelper('mod', function (value, modulus, options) {
    return value % modulus;
  });
  hbs.handlebars.registerHelper('mod', function (value, modulus) {
    return value % modulus;
  });
  hbs.handlebars.registerHelper('eq', function (a, b, options) {
    if (a === b) {
      return options.fn(this);
    } else {
      return options.inverse(this);
    }
  });

  app.get('/order/:order', (req, res) => {
    let success; const warning = app.helpers.msg(req);
    let categories; let products;
    const connection = app.dao.connectionFactory();

    const order = req.params.order;
    let totalQuantity = 0;
    const isCheckSession = req.session['cart']

    if (isCheckSession === undefined || isCheckSession === null || (isCheckSession.quantity === 0 && isCheckSession.user === null)) {
      req.session['cart'] = {
        user: null,
        cartId: [],
        products: [],
        totalQuantity: 0
      };
    } else {

      totalQuantity = isCheckSession.products.reduce((total, item) => total + item.quantity, 0);

    }
    const categoriesDAO = new app.dao.categoriesDAO(connection);
    const productsDAO = new app.dao.productsDAO(connection);
    categoriesDAO.list()
      .
      then((result) => categories = result)
      .catch((err) => warning = 'it was not possible list categories');
    let ActiveTab;
    if (order == 'rating') ActiveTab = 'rating';
    if (order == 'low-price') ActiveTab = 'low-price';
    if (order == 'price') ActiveTab = 'price';

    const productsDao = new app.dao.productsDAO(connection);
    productsDao.orderedList(order)
      .then((result) => res.render('search/index', {
        products: result,
        success, warning,
        active_tab: ActiveTab,
        user: req.session['cart'].user,
        categories,

        totalQuantity
      }))
      .catch((err) => console.log(err));
  });


  app.get('/search', async (req, res) => {
    let categories; let products;

    const searchTerm = req.query['search-term'];
    if (isTurnOnCheckSercurity === 1) {
      const isSafe = await checkSafety([searchTerm]);

      if (isSafe === 0) {
        req.session['warning'] = 'Nội dung có chứa mã độc';
        res.redirect('/'); // Redirect to home page if unsafe
        return;
      }
    }

    console.log(`searchTerm: ${searchTerm}`)
    let totalQuantity = 0;
    const isCheckSession = req.session['cart']

    if (isCheckSession === undefined || isCheckSession === null || (isCheckSession.quantity === 0 && isCheckSession.user === null)) {
      req.session['cart'] = {
        user: null,
        cartId: [],
        products: [],
        totalQuantity: 0
      };
    } else {

      totalQuantity = isCheckSession.products.reduce((total, item) => total + item.quantity, 0);

    }



    const connection = app.dao.connectionFactory();
    const productsDao = new app.dao.productsDAO(connection);


    const categoriesDAO = new app.dao.categoriesDAO(connection);
    const productsDAO = new app.dao.productsDAO(connection);
    categoriesDAO.list()
      .
      then((result) => categories = result)
      .catch((err) => warning = 'it was not possible list categories');
    productsDAO.getByProductName(searchTerm)
      .then((result) => products = result)
      .catch((err) => warning = 'it was not possible list products');
    //  console.log(products)


    setTimeout(() => {
      // Xử lý logic tìm kiếm ở đây
      res.status(200).render('search-results/index', {
        title: 'Search Result',
        categories, products,

        searchTerm,
        user: req.session['cart'].user,
        totalQuantity
      });
    }, 100);



  });
  app.get('/product/:id', async (req, res) => {
    const { id } = req.params;
    const connection = app.dao.connectionFactory();
    const productsDao = new app.dao.productsDAO(connection);
    const { cart } = req.session;
    const totalQuantity = cart ? cart.products.reduce((total, item) => total + item.quantity, 0) : 0;
    try {
      const productDetail = await productsDao.getById2(id);
      if (!productDetail) {
        return res.status(500).send('Internal Server Error');
      }

      // Call the API to fetch similar products
      const similarProductsResponse = await axios.get(`http://127.0.0.1:5000/api/knn/similar/${id}`);
      if (similarProductsResponse.status === 200) {
        const similarProducts = similarProductsResponse.data;
        const { product_name, product_image, product_rating, base_price, discount_price, promotion_price } = productDetail[0];
        const prices = productDetail.map(product => [product.discount_price, product.promotion_price]).flat();
        const highestPrice = Math.max(...prices);
        const lowestPrice = Math.min(...prices);
        const attributes = productDetail.map(item => ({
          attribute_name: item.attribute_name,
          attribute_value: item.attribute_value
        }));

        const detail = {
          product_name,
          product_image,
          product_rating,
          base_price,
          discount_price,
          promotion_price,
          highestPrice,
          lowestPrice
        };

        return res.status(200).render('product-detail/index', {
          title: 'Product Detail',
          productDetail: detail,
          totalQuantity,
          attributes,
          user: cart ? cart.user : null,
          id: productDetail[0].product_id,
          similarProducts
        });
      } else {
        // Handle other status codes if needed
        console.error('Failed to fetch similar products: Unexpected status code', similarProductsResponse.status);
        const { product_name, product_image, product_rating, base_price, discount_price, promotion_price } = productDetail[0];
        const prices = productDetail.map(product => [product.discount_price, product.promotion_price, product.base_price]).flat();
        const highestPrice = Math.max(...prices);
        const lowestPrice = Math.min(...prices);
        const attributes = productDetail.map(item => ({
          attribute_name: item.attribute_name,
          attribute_value: item.attribute_value
        }));

        const detail = {
          product_name,
          product_image,
          product_rating,
          base_price,
          discount_price,
          promotion_price,
          highestPrice,
          lowestPrice
        };

        return res.status(200).render('product-detail/index', {
          title: 'Product Detail',
          productDetail: detail,
          totalQuantity,
          attributes,
          user: cart ? cart.user : null,
          id: productDetail[0].product_id,
          similarProducts: []
        });
      }
      return;

    } catch (error) {
      console.error('Error fetching product detail:', error);
      return res.status(500).send('Internal Server Error');
    }
  });

};

// async function checkSafety(searchTerm) {
//   try {
//     const [isSafeSQLI, isSafeXSS] = await Promise.all([
//       axios.post(`http://127.0.0.1:5000/api/sqli/check`, {
//         "queries": [
//           `${searchTerm}`
//         ],
//         "max_len": 100,
//         "max_words": 5000
//       }),
//       axios.post(`http://127.0.0.1:5000/api/xss/check`, {
//         "queries": [
//           `${searchTerm}`
//         ],
//         "max_len": 100,
//         "max_words": 5000
//       })
//     ]);

//     // Check if both SQLI and XSS are safe
//     const isSafe = isSafeSQLI.data.results[0].isSafe && isSafeXSS.data.results[0].isSafe;

//     console.log("Is safe:", isSafe);
//     return isSafe;
//   } catch (error) {
//     console.error("Error:", error);
//     return false;
//   }
// }
