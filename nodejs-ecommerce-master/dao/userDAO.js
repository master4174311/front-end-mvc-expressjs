const bcrypt = require('bcrypt-nodejs');
const { v4: uuidv4 } = require('uuid')

class UserDAO {
  constructor(connection) {
    this.connection = connection;
  }
  // saveUser(username, email, password) {
  //   return new Promise((resolve, reject) => {
  //     // const cryptedPassword = bcrypt.hashSync(password);
  //     const cryptedPassword = password;

  //     this.connection
  //         .query('select email from users where email = ? OR username = ?', [email, username],
  //             (err, result) => {
  //               const emailNotExist = Object.entries(result).length === 0;
  //               if (emailNotExist) {
  //                 this.connection
  //                     .query('INSERT INTO `users` (`id`, `username`, `email`, `password`) VALUES (null, ?, ?, ?)',
  //                         [username, email, cryptedPassword],
  //                         (err, result) => {
  //                           if (err) return reject(err);
  //                           return resolve('User was created!');
  //                         });
  //               } else if (err) return reject(err);
  //               else return reject('Email or Username already exist');
  //             });
  //   });
  // }
  login(email, password) {
    return new Promise((resolve, reject) => {
      const query = `SELECT * FROM users WHERE email = '${email}' AND password = '${password}'`;
      this.connection.query(query,
        (err, result) => {
          if (err) {
            return reject(err);
          }

          // Kiểm tra kết quả trả về từ truy vấn SQL
          if (result.length === 0) {
            // Trường hợp không tìm thấy email
            return reject(null);
          } else {
            // Trường hợp tìm thấy email và password khớp
            return resolve(result);
          }
        });
    });
  }

  getUsername(email) {
    return new Promise((resolve, reject) => {
      this.connection.query('select username from users where email = ?', email,
        (err, result) => {
          if (err) return reject(err);
          return resolve(result[0].username);
        });
    });
  }

  getCustomer(email) {
    return new Promise((resolve, reject) => {


      this.connection.query('SELECT * FROM users WHERE email = ?', email, (err, result) => {
        if (err) {
          // Reject the promise if there's an error
          return reject(err);
        }
        // Resolve the promise with the query result
        resolve(result);
      });
    });
  }
  getOrders(user_id) {
    return new Promise((resolve, reject) => {
      this.connection.query('select * from orders o inner join order_transaction ot on o.id = ot.order_id where o.user_id= = ?', user_id,
        (err, result) => {
          if (err) {
            // Reject the promise if there's an error
            return reject(err);
          }
          // Resolve the promise with the query result
          resolve(result);
        });
    });
  }
  getUserById(user_id) {
    return new Promise((resolve, reject) => {
      this.connection.query('select * from users where id= ?', user_id,
        (err, result) => {
          if (err) {
            // Reject the promise if there's an error
            return reject(err);
          }
          // Resolve the promise with the query result
          resolve(result);
        });
    });
  }


  saveUser(user) {
    return new Promise((resolve, reject) => {
      // Encrypt the password before storing it in the database
      const cryptedPassword = user.password;
      const userId = uuidv4()

      // Check if the email or username already exists in the database
      this.connection.query('SELECT email FROM users WHERE email = ?', [user.email ], (err, result) => {
        if (err) {
          // Error occurred during database query
          return reject(err);
        } else {
          // Check if the result is empty, indicating that the email or username doesn't exist
          const emailNotExist = Object.entries(result).length === 0;
          if (emailNotExist) {
            var m = new Date();
            var dateString =
              m.getUTCFullYear() + "/" +
              ("0" + (m.getUTCMonth() + 1)).slice(-2) + "/" +
              ("0" + m.getUTCDate()).slice(-2) + " " +
              ("0" + m.getUTCHours()).slice(-2) + ":" +
              ("0" + m.getUTCMinutes()).slice(-2) + ":" +
              ("0" + m.getUTCSeconds()).slice(-2);

            // Insert the new user into the database
            this.connection.query('INSERT INTO `users` (`id`,`name`, `username`, `email`, `password`, `type`, `phone_number`, `address`, `address_shipping`, `creation_by`, `creation_time`, `last_modified_by`, `last_modified_time`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)', [userId, user.username, user.username, user.email, cryptedPassword, parseInt(user.type), user.phone, user.address, user.addressShipping, '', dateString, '', dateString], (err, result) => {
              if (err) {
                // Error occurred during user insertion
                return reject(err);
              } else {
                // User was successfully created
                return resolve('User was created!');
              }
            });
          } else {
            // Email or username already exists
            return reject('Email or Username already exists');
          }
        }
      });
    });
  }

}


module.exports = () => UserDAO;
