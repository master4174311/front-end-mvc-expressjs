import puppeteer from 'puppeteer';
import fs from 'fs';
import {autoScroll,waitForTimeout,DEFAULT_TIMEOUT} from "./common.js";

const pageCrawl = 'https://fptshop.com.vn/may-tinh-xach-tay?sort=ban-chay-nhat&trang=20';
const outputFilePath = 'products_laptop.json'; // Path to the output JSON file

(async () => {
    const browser = await puppeteer.launch({ headless: false });
    const page = await browser.newPage();
    await page.goto(pageCrawl);
    await page.setViewport({ width: 1080, height: 1024 });
    await page.waitForSelector('.cdt-product-wrapper', { visible: true });
    await autoScroll(page);  // set limit to 50 scrolls

    await waitForTimeout(DEFAULT_TIMEOUT);

    const products = await page.$$eval('.cdt-product', elements => {
        return elements.map(element => {
            const nameElement = element.querySelector('.cdt-product__name');
            const name = nameElement ? nameElement.textContent.trim() : '';
            
            const imageElement = element.querySelector('.cdt-product__img img');
            const image = imageElement ? imageElement.getAttribute('src') : '';
         
            const configParams = {};
            const configParamElements = element.querySelectorAll('.cdt-product__config__param span');
            configParamElements.forEach(param => {
                const title = param.getAttribute('data-title');
                const value = param.textContent.trim();
                configParams[title] = value;
            });

            const promoElement = element.querySelector('.cdt-product__show-promo');
            const promoProgress = promoElement ? promoElement.querySelector('.progress').textContent.trim() : '';
            const promoStrikePrice = promoElement ? promoElement.querySelector('.strike-price').textContent.trim() : '';

            const priceElement = element.querySelector('.cdt-product__price .tcdm');
            const priceValue = priceElement ? priceElement.textContent.trim() : '';

            return { name, image, configParams, promoProgress, promoStrikePrice, priceValue };
        });
    });

    // Save products array to a JSON file
    fs.writeFileSync(outputFilePath, JSON.stringify(products, null, 2));

    console.log(`Data saved to ${outputFilePath}`);

    await waitForTimeout(DEFAULT_TIMEOUT);
    await browser.close();
})();

