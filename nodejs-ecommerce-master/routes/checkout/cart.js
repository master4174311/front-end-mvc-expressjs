var {checkSafety} = require('../../helpers/modelsAPI');
var isTurnOnCheckSercurity = require('../../helpers/constants');

module.exports = (app) => {
  app.get('/cart', (req, res) => {
    const isCheckSession = req.session['cart']
    if (isCheckSession === undefined || isCheckSession === null || isCheckSession.quantity === 0) {
      req.session['cart'] = {
        user: null,
        cartId: [],
        products: [],
        totalQuantity: 0
      };
    }
    if (req.session['cart'].totalQuantity === 0) {
      req.session['warning'] = 'Vui lòng thêm sản phẩm vào giỏ hàng';

      return res.redirect('/')

    }
    const products = req.session['cart'] ? req.session['cart'].products : [];
    let success; const warning = app.helpers.msg(req);

    const totalQuantity = products.reduce((total, item) => total + item.quantity, 0);

    //

    if (products.length == 0) {
      res.render('home/index', {
        title: 'Cart',
        warning: 'Vui lòng thêm sản phẩm vào giỏ hàng',
      });

    }
    const totalPrice = products.reduce((acc, item) => acc + (item.product[0].price * 1), 0);
    if (!req.session['cart'].user) {
      res.render('checkout/cart', {
        title: 'Cart',
        success, warning,
        totalQuantity,
        products,
        totalPrice,
      });
      return;
    }
    res.render('checkout/cart', {
      title: 'Cart',
      success, warning,
      totalQuantity,
      products,
      totalPrice,
      user: req.session['cart'].user,
      userId: req.session['cart'].user.id
    });

  });
  app.get('/add-to-cart/:id', async (req, res) => {
    let success;
    const productId = req.params.id;
    // if (isTurnOnCheckSercurity === 1) {
    //   const isSafe = await checkSafety([productId]);
    
    //   if (isSafe === 0) {
    //     req.session['warning'] = 'Nội dung có chứa mã độc';
    //     res.redirect('/'); // Redirect to home page if unsafe
    //     return;
    //   }
    
    //   }


    const cart = req.session['cart'];
    const connection = app.dao.connectionFactory();
    const productsDao = new app.dao.productsDAO(connection);
    // Check if the product already exists in the cart
    const isProduct = cart.products.find(item => item.id === productId);

    if (isProduct) {
      // If the product already exists, increase its quantity
      isProduct.quantity++;
    } else {
      const resultProduct = await productsDao.getById(productId);
      // If the product doesn't exist, add it to the cart
      const product = {
        product: resultProduct,
        quantity: 1
      };
      cart.products.push(product);
    }

    // Calculate total quantity in the cart
    cart.totalQuantity = cart.products.reduce((total, item) => total + item.quantity, 0);

    // Update the session with the modified cart
    req.session['cart'] = cart;

    res.redirect('/cart');
    // res.status(200)
  });
  app.post('/add-to-cart/:id', async (req, res) => {
    let success;
    const productId = req.params.id;
    const cart = req.session['cart'];
    const connection = app.dao.connectionFactory();
    const productsDao = new app.dao.productsDAO(connection);

    try {
      // Check if the product already exists in the cart
      const existingProductIndex = cart.products.findIndex(item => item.product[0].id === productId);

      if (existingProductIndex !== -1) {
        // If the product already exists, increase its quantity
        cart.products[existingProductIndex].quantity++;
      } else {
        // If the product doesn't exist, add it to the cart
        const resultProduct = await productsDao.getById(productId);
        const product = {
          product: resultProduct,
          quantity: 1
        };
        cart.products.push(product);
      }

      // Calculate total quantity in the cart
      cart.totalQuantity = cart.products.reduce((total, item) => total + item.quantity, 0);

      // Update the session with the modified cart
      req.session['cart'] = cart;

      // Send success response
      res.status(200).send("success");
    } catch (error) {
      // Handle errors
      console.error('Error adding product to cart:', error);
      res.status(500).send("error");
    }
  });

  app.post('/remove-from-cart/:id', (req, res) => {
    const productId = req.params.id;
    const cart = req.session['cart'];
    console.log(JSON.stringify(cart))

    // Check if the product exists in the cart
    // Find the index of the product with the matching ID
    let index = -1;
    for (let i = 0; i < cart.products.length; i++) {
      if (cart.products[i].product[0].id === productId) {
        index = i;
        break;
      }
    } if (index !== -1) {
      // Remove the product from the cart
      cart.products.splice(index, 1);
      // Update the total quantity in the cart if needed
      cart.totalQuantity -= 1;
      // Optionally, perform other operations like updating the total price
      const totalPrice = cart.products.reduce((total, item) => total + item.price, 0);
      // Send a success response
      res.status(200).send({
        status: "success", data: {
          indexItem: index,

          productId
        }
      });
    } else {
      // If the product is not found in the cart, send an error response
      res.status(404).send('Product not found in the cart');
    }
  });
}
