export async function autoScroll(page){
    await page.evaluate(async () => {
        await new Promise((resolve) => {
            var totalHeight = 0;
            var distance = 100;
            var timer = setInterval(() => {
                var scrollHeight = document.body.scrollHeight;
                window.scrollBy(0, distance);
                totalHeight += distance;

                if(totalHeight >= scrollHeight - window.innerHeight){
                    clearInterval(timer);
                    resolve();
                }
            }, 100);
        });
    });
}

export async function waitForTimeout(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

export  const DEFAULT_TIMEOUT = 1000

export function generateRandomRating() {
    // Sinh một số ngẫu nhiên từ 0 đến 5 (bao gồm cả 0 và 5)
    return Math.floor(Math.random() * 6); // Math.floor làm tròn số xuống
  }
  
  