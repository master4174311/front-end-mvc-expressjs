// Define the constant object
const initialCartState = {
    user: {},
    cartId: [],
    products: [],
    totalQuantity: 0
};

// Export the constant object
module.exports = initialCartState;
