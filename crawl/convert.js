import { readFileSync, writeFileSync } from 'fs';
import {autoScroll,waitForTimeout,DEFAULT_TIMEOUT,generateRandomRating} from "./common.js";
import { v4 as uuidv4 } from 'uuid';

const outputFilePath = 'products_laptop_sql.json'; // Path to the output JSON file

(async () => {
    const products = [];
    const sql = [];
    const datas = await readFileSync('products_laptop.json', 'utf-8');
    const parseJson = JSON.parse(datas);

    parseJson.forEach((element, index) => {
        const idUUID = uuidv4();
        const { name, image, configParams, promoProgress, promoStrikePrice, priceValue } = element;
        // const id = `DT-${index+1}`;
        const renamedParams = {
            "Screen": configParams["Màn hình"],
            "CPU": configParams["CPU"],
            "RAM": configParams["RAM"],
            "Storage": configParams["Ổ cứng"],
            "Graphics": configParams["Đồ họa"],
            "Weight": configParams["Trọng lượng"]
        };
        
        const rating = generateRandomRating();
        const priceLast = priceValue === ''|| priceValue === null ? promoStrikePrice.replace(/\d+ ngày \d+:\d+:\d+$/, '') : priceValue.replace(/\d+ ngày \d+:\d+:\d+$/, '')
        // Remove unwanted part from promoStrikePrice
        const cleanedPromoStrikePrice = promoStrikePrice.replace(/\d+ ngày \d+:\d+:\d+$/, '');
        // const query =  `(${index},'${name}', '${rating}', '${priceLast.replace(/\./g, "").replace(" đ", "")}', '${image}', 2)`
        if (name.includes('Asus')) {
                    // const query =  `(${index},'${name}', '${rating}', '${priceLast.replace(/\./g, "").replace(" đ", "")}', '${image}', 2)`

        }
       sql.push(query);
        products.push({ idUUID,rating,name, image, configParams: renamedParams, promoProgress, promoStrikePrice: cleanedPromoStrikePrice, priceValue });
    });


     await writeFileSync(outputFilePath, JSON.stringify(sql, null, 2));
})();
