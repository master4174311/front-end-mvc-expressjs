INSERT INTO ecommerce.`attributes` (name,icons,creation_by,creation_time,last_modified_by,last_modified_time) VALUES
	 ('Screen','screen.png','system','2024-03-09 10:38:50','system','2024-03-09 10:38:50'),
	 ('CPU','processor.png','system','2024-03-09 10:38:50','system','2024-03-09 10:38:50'),
	 ('RAM','ram.png','system','2024-03-09 10:38:50','system','2024-03-09 10:38:50'),
	 ('Storage','harddisk.png','system','2024-03-09 10:38:50','system','2024-03-09 10:38:50'),
	 ('Graphics','vga-card.png','system','2024-03-09 10:38:50','system','2024-03-09 10:38:50'),
	 ('Weight','kilogram.png','system','2024-03-09 10:38:50','system','2024-03-09 10:38:50');
