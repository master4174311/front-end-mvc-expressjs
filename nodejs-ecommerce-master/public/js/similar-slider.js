let similarIndex = 0;
const similarSlides = document.querySelectorAll('.similar-slide');
const similarTabs = document.querySelectorAll('#similar-slider-tabs .tab');

function showSimilarSlide() {
  similarSlides.forEach(slide => {
    slide.style.display = 'none';
  });
  similarSlides[similarIndex].style.display = 'block';
}

function showSimilarTab() {
  similarTabs.forEach(tab => {
    tab.classList.remove('current-tab');
  });
  similarTabs[similarIndex].classList.add('current-tab');
}

function nextSimilarSlider() {
  similarIndex++;
  if (similarIndex >= similarSlides.length) {
    similarIndex = 0;
  }
  showSimilarSlide();
  showSimilarTab();
}

function previusSimilarSlider() {
  similarIndex--;
  if (similarIndex < 0) {
    similarIndex = similarSlides.length - 1;
  }
  showSimilarSlide();
  showSimilarTab();
}

// Initialize slider
showSimilarSlide();
showSimilarTab();
// let items = document.querySelectorAll('.carousel .carousel-item')

// 		items.forEach((el) => {
// 			const minPerSlide = 2
// 			let next = el.nextElementSibling
// 			for (var i=1; i<minPerSlide; i++) {
// 				if (!next) {
//             // wrap carousel by using first child
//             next = items[0]
//         }
//         let cloneChild = next.cloneNode(true)
//         el.appendChild(cloneChild.children[0])
//         next = next.nextElementSibling
//     }
// })