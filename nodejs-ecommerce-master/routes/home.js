var { getModelsAsync ,getTrainKNNModelsAsync,
  getTrainXSSRNNModelsAsync,getTrainSQLiRNNModelsAsync} = require('../helpers/modelsAPI');

module.exports = (app) => {
  app.get('/', (req, res) => {

    let totalQuantity = 0
    let user = null;
    const isCheckSession = req.session['cart']
    if (isCheckSession === undefined || isCheckSession === null || (isCheckSession.quantity === 0 && isCheckSession.user === null)) {
      req.session['cart'] = {
        user: null,
        cartId: [],
        products: [],
        totalQuantity: 0
      };
    } else {
      const productsSession = req.session['cart'] ? req.session['cart'].products : [];
      user = req.session['cart'].user
      totalQuantity = productsSession.reduce((total, item) => total + item.quantity, 0);

    }

    let success; let warning = app.helpers.msg(req);
    let categories; let products;
    const connection = app.dao.connectionFactory();
    const categoriesDAO = new app.dao.categoriesDAO(connection);
    const productsDAO = new app.dao.productsDAO(connection);

    console.log(req.session['cart'].user)
    categoriesDAO.list()
      .
      then((result) => categories = result)
      .catch((err) => warning = 'it was not possible list categories');
    productsDAO.list(9)
      .then((result) => products = result)
      .catch((err) => warning = 'it was not possible list products');
    //  console.log(products)
    if (user === null) {
      setTimeout(() => {
        res.status(200).render('home/index', {
          title: 'Home',
          categories, products,
          success, warning,
          totalQuantity,
          // user: req.session['cart'],
        });
      }, 300);
      return;
    }

    setTimeout(() => {
      res.status(200).render('home/index', {
        title: 'Home',
        categories, products,
        success, warning,
        totalQuantity,
        user: user
        // user: req.session['cart'],
      });
    }, 100);
  });




  app.get('/admin/products', (req, res) => {
    const connection = app.dao.connectionFactory();
    const productsDAO = new app.dao.productsDAO(connection);

    // Get parameters from DataTable request
    const draw = req.query.draw; // Draw counter
    const start = parseInt(req.query.start); // Paging start index
    const length = parseInt(req.query.length); // Number of records per page

    productsDAO.listProductsWithPagination(start, length)
      .then((result) => {
        // Send JSON response with the list of products
        res.json({
          draw: draw,
          recordsTotal: result.totalRecords,
          recordsFiltered: result.filteredRecords,
          data: result.data
        });
      })
      .catch((err) => {
        console.error(err);
        res.status(500).json({ error: 'Unable to fetch products' });
      });
  });


  app.get('/admin/orders', (req, res) => {
    const connection = app.dao.connectionFactory();
    const productsDAO = new app.dao.productsDAO(connection);

    // Get parameters from DataTable request
    const draw = req.query.draw; // Draw counter
    const start = parseInt(req.query.start); // Paging start index
    const length = parseInt(req.query.length); // Number of records per page

    productsDAO.listOrdersWithPagination(start, length)
      .then((result) => {
        // Send JSON response with the list of products
        res.json({
          draw: draw,
          recordsTotal: result.totalRecords,
          recordsFiltered: result.filteredRecords,
          data: result.data
        });
      })
      .catch((err) => {
        console.error(err);
        res.status(500).json({ error: 'Unable to fetch orders' });
      });
  });


  app.get('/admin/categories', (req, res) => {
    const connection = app.dao.connectionFactory();
    const productsDAO = new app.dao.productsDAO(connection);

    // Get parameters from DataTable request
    const draw = req.query.draw; // Draw counter
    const start = parseInt(req.query.start); // Paging start index
    const length = parseInt(req.query.length); // Number of records per page

    productsDAO.listCategoriesWithPagination(start, length)
      .then((result) => {
        // Send JSON response with the list of products
        res.json({
          draw: draw,
          recordsTotal: result.totalRecords,
          recordsFiltered: result.filteredRecords,
          data: result.data
        });
      })
      .catch((err) => {
        console.error(err);
        res.status(500).json({ error: 'Unable to fetch categories' });
      });
  });


  app.post('/admin/models', async (req, res) => {
    const data = await getModelsAsync()
    res.status(200).send({
      status: "success", data: {
        data
      }
    });
  });

  app.post('/admin/trainKNNModel', async (req, res) => {
    const k = req.body.kCluster;
    const data = await getTrainKNNModelsAsync(k)
    res.status(200).send({
      status: "success", data: {
        data
      }
    });
  });


  app.post('/admin/trainSQLiRNNModel', async (req, res) => {
    try {
        const body = req.body;
        const data = await getTrainSQLiRNNModelsAsync(body);
        res.status(200).send({
            status: 'success',
            data: data
        });
    } catch (error) {
        console.error('Error:', error);
        res.status(500).send({
            status: 'error',
            message: 'Internal server error'
        });
    }
});

app.post('/admin/trainXssRNNModel', async (req, res) => {
  try {
      const body = req.body;
      const data = await getTrainXSSRNNModelsAsync(body);
      res.status(200).send({
          status: 'success',
          data: data
      });
  } catch (error) {
      console.error('Error:', error);
      res.status(500).send({
          status: 'error',
          message: 'Internal server error'
      });
  }
});




};