module.exports = (app) => {
app.get('/filter/:id', (req, res) => {
    let categories; let products;
    let totalQuantity=0;
    const isCheckSession = req.session['cart'] 

    if (isCheckSession === undefined || isCheckSession === null || (isCheckSession.quantity === 0 && isCheckSession.user === null) ) {
      req.session['cart'] = {
        user: null,
        cartId: [],
        products: [],
        totalQuantity: 0
    };
    }else{
  
       totalQuantity = isCheckSession.products.reduce((total, item) => total + item.quantity, 0);
  
    }
    const id = req.params.id;
    const connection = app.dao.connectionFactory();

   
    const categoriesDAO = new app.dao.categoriesDAO(connection);
    const productsDAO = new app.dao.productsDAO(connection);
    categoriesDAO.list()
        .
        then((result) => categories = result)
        .catch((err) => warning = 'it was not possible list categories');
    productsDAO.getByCategoryId(id)
        .then((result) => products = result)
        .catch((err) => warning = 'it was not possible list products');
      //  console.log(products)


    setTimeout(() => {
        res.status(200).render('filter/index', {
          title: 'categories',
          categories, products,
          totalQuantity,
          user: req.session['cart'].user,
        });
      }, 100);
});

}