function onTrain() {
    // Show confirmation popup
    if (confirm('Are you sure you want to train with these parameters?')) {
        
        // Retrieve values from modal inputs
        var maxWords = document.getElementById('maxWords').value;
        var maxLen = document.getElementById('maxLen').value;
        var testSize = document.getElementById('testSize').value;
        var randomState = document.getElementById('randomState').value;
        var epochs = document.getElementById('epochs').value;
        var batchSize = document.getElementById('batchSize').value;
        var validationSplit = document.getElementById('validationSplit').value;
        var outputDim = document.getElementById('outputDim').value;
        var unitRNN = document.getElementById('unitRNN').value;
        var unitDense = document.getElementById('unitDense').value;
        
        // Use the retrieved values as needed
        console.log('Max Words:', maxWords);
        console.log('Max Length:', maxLen);
        console.log('Test Size:', testSize);
        console.log('Random State:', randomState);
        console.log('Epochs:', epochs);
        console.log('Batch Size:', batchSize);
        console.log('Validation Split:', validationSplit);
        console.log('Output Dim:', outputDim);
        console.log('Unit RNN:', unitRNN);
        console.log('Unit Dense:', unitDense);

        // Call a function to process the retrieved values
        processData(maxWords, maxLen, testSize, randomState, epochs, batchSize, validationSplit, outputDim, unitRNN, unitDense);
    }
}

function onTrainKnn(){
    
}

function processData(maxWords, maxLen, testSize, randomState, epochs, batchSize, validationSplit, outputDim, unitRNN, unitDense) {
    // Process the retrieved values here
    // For example, you can make an AJAX request to send the data to the server
}