
// Calculate total price and update the displayed value
function calculateTotalPrice() {
  let totalPrice = 0;
  const priceElements = document.querySelectorAll('.price-value');
  const quantityInputs = document.querySelectorAll('input[type="number"]');

  priceElements.forEach((priceElement, index) => {
    const price = parseFloat(priceElement.dataset.price);
    const quantity = parseInt(quantityInputs[index].value);
    totalPrice += price * quantity;
  });

  document.getElementById('total-price').textContent = totalPrice //.toFixed(2); // Assuming totalPrice is in decimal format
}

// Add event listeners to input elements to update total price on change
document.addEventListener('DOMContentLoaded', function () {
  const quantityInputs = document.querySelectorAll('input[type="number"]');
  quantityInputs.forEach(input => {
    input.addEventListener('change', calculateTotalPrice);
  });
});

// Handle click event for "Proceed to Checkout" button
function proceedToCheckout(user) {
  
  // Check if user is undefined, null, or an empty string
  if (!user) {
      // Redirect to the sign-in page if user is not logged in
      window.location.href = '/sign-in';
  } else {
      // Proceed to the checkout page if user is logged in
      console.log('proceedToCheckout');
      // Add logic here to navigate to the order confirmation page
      // For example:
      window.location.href = '/checkout/confirm';
  }
}

// Hàm thêm sản phẩm vào giỏ hàng
function addToCart(productId) {
  // Gửi yêu cầu AJAX để thêm sản phẩm vào giỏ hàng
  // Đặt URL của đường dẫn API để thêm sản phẩm vào giỏ hàng
  const addToCartUrl = `/add-to-cart/${productId}`;
  // Gửi yêu cầu POST thông qua AJAX
  // fetch(addToCartUrl, {
  //     method: 'POST', // Phương thức POST
  //     headers: {
  //         'Content-Type': 'application/json' // Định dạng dữ liệu là JSON
  //     },
  //     body: JSON.stringify({ productId }) // Gửi dữ liệu sản phẩm ID dưới dạng JSON
  // })
  // .then(response => {
  //     // Kiểm tra trạng thái của phản hồi
  //     if (response.ok) {
  //         // Nếu phản hồi thành công, cập nhật giỏ hàng trên giao diện người dùng
  //         updateCart();
  //     } else {
  //         // Nếu có lỗi, hiển thị thông báo cho người dùng
  //         console.error('Failed to add product to cart');
  //     }
  // })
  // .catch(error => {
  //     // Nếu có lỗi trong quá trình gửi yêu cầu, hiển thị thông báo cho người dùng
  //     console.error('Error adding product to cart:', error);
  // });
}

// Hàm cập nhật tổng giá trị khi số lượng sản phẩm thay đổi
function updateTotalPrice() {
  // Lặp qua các hàng trong bảng
  let totalPrice = 0;
  document.getElementById('cart-table').querySelectorAll('tbody tr').forEach(row => {
    // Lấy giá và số lượng từ từng hàng
    const price = parseFloat(row.querySelector('.price-value').getAttribute('data-price'));
    const quantity = parseInt(row.querySelector('input[type="text"]').value);
    // Tính toán tổng giá trị cho từng sản phẩm và cộng dồn vào tổng giá trị
    totalPrice += price * quantity;
  });
  let price = parseFloat(totalPrice);
  // Format giá tiền thành chuỗi có dạng tiền tệ
  price = price.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' });
  // Cập nhật tổng giá trị mới lên trên giao diện
  document.getElementById('total-price').textContent = price;
}

// Hàm để tăng số lượng sản phẩm
function increaseQuantity(productId) {
  
  // Lấy số lượng hiện tại từ trường nhập
  let quantityInput = document.querySelector(`#quantity-${productId}`);
  let currentQuantity = parseInt(quantityInput.value);
  // Tăng số lượng lên 1
  currentQuantity++;
  // Cập nhật số lượng mới vào trường nhập
  quantityInput.value = currentQuantity;
  fetch(`/add-to-cart/${productId}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ productId })
  })
  // Gọi hàm updateTotalPrice để cập nhật tổng giá trị dựa trên số lượng mới
  updateTotalPrice(productId, currentQuantity);
}

// Hàm để tăng số lượng sản phẩm
function decreaseQuantity(productId) {
  
  // Lấy số lượng hiện tại từ trường nhập
  let quantityInput = document.querySelector(`#quantity-${productId}`);
  let currentQuantity = parseInt(quantityInput.value);
  // Giảm số lượng đi 1
  currentQuantity--;
  // Cập nhật số lượng mới vào trường nhập
  quantityInput.value = currentQuantity;

    // Kiểm tra nếu số lượng giảm xuống 0 thì hiển thị cửa sổ pop-up
    if (currentQuantity === 0) {
      // Hiển thị cửa sổ pop-up
      alert('Bạn có muốn xóa sản phẩm này khỏi giỏ hàng không?');
      // Nếu người dùng xác nhận xóa, gửi yêu cầu xóa sản phẩm tới server
      fetch(`/remove-from-cart/${productId}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ productId })
      })
        .then(response => {
          if (response.ok) {
            
  
            // Nếu phản hồi thành công, cập nhật giỏ hàng trên giao diện người dùng
            // updateCart();
            return response.json();
          } else {
            // Nếu có lỗi, hiển thị thông báo cho người dùng
            console.error('Failed to remove product from cart');
          }
        }).then(data => {
          
          console.log(data.data.indexItem);
  
          removeRow(data.data.indexItem)
          updateTotalPrice(productId, currentQuantity);

        })
        .catch(error => {
          // Nếu có lỗi trong quá trình gửi yêu cầu, hiển thị thông báo cho người dùng
          console.error('Error removing product from cart:', error);
        });
    }
  // Gọi hàm updateTotalPrice để cập nhật tổng giá trị dựa trên số lượng mới
  updateTotalPrice(productId, currentQuantity);


}
function removeRow(indexToRemove) {
  // Get the row to remove by its index
  
var table = document.getElementById('cart-table');
var rowToRemove = table.rows[indexToRemove+1];

// Remove the row from the table
if (rowToRemove) {
    rowToRemove.remove();
} else {
    console.error('Row not found');
}
}


