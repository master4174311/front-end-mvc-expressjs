const divsPrice = document.querySelectorAll('.price-value');

divsPrice.forEach((divPrice) => {
  let price = parseFloat(divPrice.getAttribute('data-price'));
  price = price.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' });
  divPrice.innerText = price;
});
